package t3_201820;

import model.data_structures.Queue;
import model.data_structures.Stack;

import static org.junit.Assert.assertEquals;

import org.junit.Assert.*;

import org.junit.Test;

public class ElTest {

	final static String P1 = "Esta";
	final static String P2 = "cosa";
	final static String P3 = "funciona";
	final static String P4 = "Bien";
	private Stack<String> S;
	private Queue<String> Q;
	
	@Test
	public void testStack()
	{
		S.push(P1);
		S.push(P2);
		S.push(P3);
		S.push(P4);
		assertEquals(P4, S.pop());
		assertEquals(P3, S.pop());
		assertEquals(P2, S.pop());
		assertEquals(P1, S.pop());
	}
	
	@Test
	public void testQueue()
	{
		Q.enqueue(P1);
		Q.enqueue(P2);
		Q.enqueue(P3);
		Q.enqueue(P4);
		assertEquals(P1, Q.dequeue());
		assertEquals(P2, Q.dequeue());
		assertEquals(P3, Q.dequeue());
		assertEquals(P4, Q.dequeue());
	}
}
