package controller;

import api.IDivvyTripsManager;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Stack;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadStations() {

	}
	
	public static void loadTrips() {
		
		manager.loadTrips("./data/Divvy_Trips_2017_Q3.csv");
	}
		
	public static IDoublyLinkedList <String> getLastNStations (int bicycleId, int n) {
		return manager.getLastNStations(bicycleId, n);
	}
	
	public static VOTrip customerNumberN (int stationID, int n) {
		return manager.customerNumberN(stationID, n);
	}
	
}
