package api;

import model.data_structures.IDoublyLinkedList;
import model.data_structures.Stack;
import model.vo.VOTrip;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IDivvyTripsManager {

	/**
	 * Method to load the Divvy trips of the STS
	 * @param tripsFile - path to the file 
	 */
	void loadTrips(String tripsFile);
	
	
	/**
	 * Method to load the Divvy Stations of the STS
	 * @param stationsFile - path to the file 
	 */
	void loadStations(String stationsFile);
	
	/**
	* Method to search for the last N stations visited by a bike
	* @param bicycleId bike's Id
	* @param n number of last stations to search
	* @return List with the name of the stations. It can be a empty list.
	*/
	IDoublyLinkedList <String> getLastNStations (int bicycleId, int n);
	
	/**
	* Method to search for the N-service that finished in a station
	* @param stationId station's Id
	* @param n number of  service to search. It can be null.
	*/
	VOTrip customerNumberN (int stationID, int n);

	
}
