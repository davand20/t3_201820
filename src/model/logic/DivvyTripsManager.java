package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

import api.IDivvyTripsManager;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class DivvyTripsManager <T> implements IDivvyTripsManager {

	private Stack<String> S;
	private Queue<String> Q;
	
	   private static String[] removeTrailingQuotes(String[] fields) {

		      String result[] = new String[fields.length];

		      for (int i=0;i<result.length;i++){
		         result[i] = fields[i].replaceAll("^"+"\"", "").replaceAll("\""+"$", "");
		      }
		      return result;
		   }
	public DivvyTripsManager()
	{
	       Node n = new Node(null);
		   S = new Stack(n);
		   Q = new Queue(null);
	}
	   
	public void loadStations (String stationsFile) {
		// TODO Auto-generated method stub
		
	}
	
	public void loadTrips (String tripsFile) {
	    BufferedReader br = null;
	    try {
	       br =new BufferedReader(new FileReader(tripsFile));
	       String line = br.readLine();
	       while (null!=line) {
		          S.push(line);
		          Q.enqueue(line);
		          line = br.readLine();
	       }
	       
	    } catch (Exception e) {
	    } finally {
	       if (null!=br) {
	          try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	       }
	    }
	}
	
	@Override
	public IDoublyLinkedList <String> getLastNStations (int bicycleId, int n) {
		// TODO Auto-generated method stub
		int contador = 0;
		String[] este = null;
		IDoublyLinkedList<String> L = new DoublyLinkedList();
		Q.dequeue();
		while(contador < n)
		{ 
		String actual = Q.dequeue();
		String[] buscado = actual.split(",");
        buscado = removeTrailingQuotes(buscado);
        if( Integer.parseInt(buscado[3]) == bicycleId )
		{
			contador++;
			este = buscado;
	        L.agregar(este[8]);
		}

		}

		//L.agregar(Q.dequeue());
		return L;
	}

	@Override
	public VOTrip customerNumberN (int stationID, int n) {
		int contador = 0;
		VOTrip vo = null;
		String[] este = null;
		while ( contador < n )
		{			
			String actual =  S.pop();
			
			S.pop().toString();
			String[] buscado = actual.split(",");
	        buscado = removeTrailingQuotes(buscado);
			if( Integer.parseInt(buscado[7]) == stationID )
			{
				contador++;
			}
			este = buscado;
		}
		vo = new VOTrip( Integer.parseInt(este[0]), Double.parseDouble(este[4]), este[6], este[8]);
		
		return vo;
	}	

}
