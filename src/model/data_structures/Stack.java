package model.data_structures;

import java.util.Iterator;

public class Stack<T> implements IStack {

	private Node tope;
	private int N;
	
	public Stack ( Node n )
	{
		tope = n;
	}
	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return N;
	}

	@Override
	public void push(Object t) {
		Node n = new Node( t );
		n.cambiarSiguiente( tope );
		tope = n;
		N++;
	}

	@Override
	public T pop() {
		T elem = (T) tope.darElem();
		tope = tope.darSiguiente();
		N--;
		return elem;
	}
	
	public Node darTope()
	{
		return tope;
	}

}
