package model.data_structures;

import java.util.Iterator;

public class Queue<T> implements IQueue{

	private Node primero;
	private Node ultimo;
	private int N;
	
	public Queue(Node n)
	{
		primero = n;
	}
	
	@Override
	public Iterator iterator() {
		return null;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return primero == null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return N;
	}

	@Override
	public void enqueue(Object t) {
		// TODO Auto-generated method stub
		Node actual = ultimo;
		ultimo = new Node(t);
		if (isEmpty())
			primero = ultimo;
		else
			actual.cambiarSiguiente(ultimo);
		N++;
	}

	@Override
	public T dequeue() {
		// TODO Auto-generated method stub
		T elem = (T) primero.darElem();
		primero = primero.darSiguiente();
		if ( isEmpty()) ultimo = null;
		N--;
		return elem;
	}
	

}
