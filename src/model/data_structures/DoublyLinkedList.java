package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedList<T> implements IDoublyLinkedList{

	private int N;
	private Node<T> primero;
	@Override
	public Iterator iterator() {		
		return new Iterator<T>() {

			
			int i = 0;
			Node<T> actual = null;
			@Override
			public boolean hasNext() {
				if(actual == null && primero != null) {
					return true;
				}else if(actual.darSiguiente()!=null) {
					return true;
				}
				return false;
			}

			@Override
			public T next() {
				// TODO Auto-generated method stub
				T temp = null;
				if(actual == null) {
					actual = primero;
					temp = primero.darElem();					
				}else {					
					temp = actual.darSiguiente().darElem();
					actual = actual.darSiguiente();
				}
				return temp;
			}
		};
	}
	
	public DoublyLinkedList()
	{
		primero =null;
	}

	@Override
	public void agregar(Object t) {
		// TODO Auto-generated method stub
		Node nuevo = new Node(t);
		if( primero == null)
		{
			primero = nuevo;
			N++;
		}
		else
		{
			Node actual = primero;
			while( actual.darSiguiente() != null)
			{
				actual = actual.darSiguiente();
			}
		    actual.cambiarSiguiente(nuevo);
		}
	}

	@Override
	public Node darTope() {
		
		return primero;
	}
	       
	

}
