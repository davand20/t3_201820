package model.data_structures;

import java.util.Iterator;

public class Node<T> {

	private T elem;
	private Node<T> anterior;
	private Node<T> siguiente;
	
	public Node ( T e )
	{
		elem = e;
		siguiente = null;
		anterior = null;
	}
	
	public void cambiarAnterior(Node<T> nuevo)
	{
		anterior = nuevo;
	}

	public void cambiarSiguiente(Node<T> nuevo)
	{
		siguiente = nuevo;
	}

	public Node<T> darAnterior( )
	{
		return anterior;
	}
	
	public Node<T> darSiguiente( )
	{
		return siguiente;
	}
	
	public T darElem( )
	{
		return elem;
	}

	
}
