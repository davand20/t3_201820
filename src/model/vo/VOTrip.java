package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	private int id;
	private double sec;
	private String desde;
	private String llegada;
	
	public VOTrip( int i, double t, String d, String ll)
	{
		id = i;
		sec = t;
		desde = d;
		llegada = ll;
	}
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return id;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return sec;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return desde;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return llegada;
	}
}
